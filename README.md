# IOWseq000016_PaintSed1

16S amplicon analysis for the PaintSed project (PI: Alex Tagg). Samples are from a sediment incubation experiment with different combinations of paint particles.

### Running 16S analysis

```
# Initial FastQC check
cd /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/FastQC
conda activate qc-env
cut -f2 ../../Repos/IOWseq000016_PaintSed1/sample_list.txt | parallel -j10 'fastqc {} -o .'
cut -f3 ../../Repos/IOWseq000016_PaintSed1/sample_list.txt | parallel -j10 'fastqc {} -o .'

cd /bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/
conda activate snakemake

# trimming
snakemake -s /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/Repos/IOWseq000016_PaintSed1/Snakefile_trimming --use-conda -j 60

# denoising
snakemake -s /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/Repos/IOWseq000016_PaintSed1/Snakefile_denoising --use-conda -j 60
# The expected fragment length range was manually adjusted after the snakemake run finished based on
# Length_distribution_PaintSed1.txt information. ASVs shorter than 251 and longer than 257 were discarded.

# classification
# Here, a cutsom parsing for the blast output against NT was used to select all hits wil equally good blast statistics (all best hits)
snakemake -s /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/Repos/IOWseq000016_PaintSed1/Snakefile_classification --use-conda -j 60
```


### Statistical data analysis

The statistical data analysis presented in Tagg et al. 202X, incl. the code for the figure generation, is documented in: [file](link)

The prediction of metabolic functions for the microbial communities using picrust2 and paprica is documented in: [metabolic_prediction.sh](https://git.io-warnemuende.de/bio_inf/IOWseq000016_PaintSed1/src/branch/master/metabolic_prediction.sh)

To explore the origin of the bacterial strains most similar to the ASVs indicative of antifouling paints, the isolation source of the best blast hit was retrieved usinf the NCBI E-utils.

```
conda activate antismash-6.1.1
cd /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/run1_16S
sed '1d' paintindic_asvids.csv | grep -F -w -f - tax_blast_nt_PaintSed1.txt > paintindic_asvids_blast_nt.txt
cut -f2 paintindic_asvids_blast_nt.txt | cut -d'|' -f4 | sort | uniq | while read line
do
  efetch -db nuccore -id $line -format docsum | grep -A1 "<SubType>" | sed "s/^/$line\t/" >> paintindic_asvids_efetch_out.txt
done
grep -A1 "isolation_source" paintindic_asvids_efetch_out.txt | sed '/^--$/d' | sed -e 's/<SubType>/tag\t/' -e 's/<SubName>/value\t/' -e 's/<\/SubName>//' -e 's/<\/SubType>//' > paintindic_asvids_isolation_source.txt
paste <(grep -A1 "isolation_source" paintindic_asvids_efetch_out.txt | sed '/^--$/d' | grep "<SubType>" | sed -e 's/\t *<SubType>/\t/' -e 's/<\/SubType>//') <(grep -A1 "isolation_source" paintindic_asvids_efetch_out.txt | sed '/^--$/d' | grep "<SubName>" | sed -e 's/\t *<SubName>/\t/' -e 's/<\/SubName>//') > paintindic_asvids_isolation_source.txt

# For further parsing, switch to R
conda activate r-4.2.2
require(tidyverse)
dat <- read.table("paintindic_asvids_isolation_source.txt", h = F, sep = "\t") 
dat$isolation_source <- sapply(
  1:nrow(dat), 
  function(x) {
    strsplit(dat$V4[x], split = "|", fixed = T)[[1]][strsplit(dat$V2[x], split = "|", fixed = T)[[1]] == "isolation_source"]
  }
)
rownames(dat) <- dat$V1
blastout <- read.table("paintindic_asvids_blast_nt.txt", sep = "\t", h = F)
blastout$uid <- sapply(strsplit(blastout$V2, split = "|", fixed = T), function(x) x[4])
blastout$isolation_source <- dat[blastout$uid, "isolation_source"]
paintindic_isolation_source_summary <- by(
  blastout$isolation_source,
  blastout$V1,
  function(x) {
    paste(unique(x[!is.na(x)]), collapse = ";")
  }
) %>% 
  c() %>% 
  as.data.frame()
colnames(paintindic_isolation_source_summary) <- "isolation_source"
paintindic_isolation_source_summary <- paintindic_isolation_source_summary %>% filter(isolation_source != "")
write.table(paintindic_isolation_source_summary, "paintindic_isolation_source_summary.txt", quote = F, sep = "\t", col.names = F)
# in cases where no isolation source was available, the database entry was checked manually
```

