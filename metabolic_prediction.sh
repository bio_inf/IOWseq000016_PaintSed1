#################################################################
# copy number correction and metabolic prediction with picrust2 #
#################################################################

# running pipeline step by step

# potential biases: 
#   picrust uses IMG genomes as reference for 16S copy number --> Beier et al. 2022 showed that those copy numbers may be underestimated compared to rrnDB
#   picrust uses MinPath and assumes that each pathway is represented by at least 3 reactions. This is not the case for all pathways.

cd /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/metabolic_prediction/picrust_out
conda activate picrust2-2.5.1

# step 0: prepare fasta file
cp ../../run1_16S/asv_classified_PaintSed1.txt asv_for_picrust.txt
# manually insert column header for ASV names (1st column)
cut -f1 asv_for_picrust.txt | sed '1d' | grep -A1 -w -F -f - ../../run1_16S/seqs_PaintSed1.fasta | sed '/--/d' > seqs_for_picrust.fasta

# step 1: place sequences in tree
place_seqs.py -s seqs_for_picrust.fasta -o placed_seqs.tre -p 38 --intermediate placement_working
# Warning - 105 input sequences aligned poorly to reference sequences (--min_align option specified a minimum proportion of 0.8 aligning to reference sequences). These input sequences will not be placed and will be excluded from downstream steps.
# This is the set of poorly aligned input sequences to be excluded: asv_16s_18487, asv_16s_19021, asv_16s_12291, asv_16s_18809, asv_16s_16642, asv_16s_17595, asv_16s_18457, asv_16s_16703, asv_16s_18884, asv_16s_17819, asv_16s_18877, asv_16s_18126, asv_16s_17482, asv_16s_18179, asv_16s_17998, asv_16s_17687, asv_16s_9674, asv_16s_15592, asv_16s_17313, asv_16s_18999, asv_16s_18893, asv_16s_18548, asv_16s_18657, asv_16s_18583, asv_16s_14403, asv_16s_18858, asv_16s_18978, asv_16s_7240, asv_16s_18804, asv_16s_17995, asv_16s_9472, asv_16s_18833, asv_16s_18680, asv_16s_18576, asv_16s_18568, asv_16s_14529, asv_16s_17212, asv_16s_18449, asv_16s_17704, asv_16s_19031, asv_16s_14913, asv_16s_17811, asv_16s_18440, asv_16s_16802, asv_16s_18609, asv_16s_19023, asv_16s_18469, asv_16s_19006, asv_16s_18080, asv_16s_16100, asv_16s_18956, asv_16s_18580, asv_16s_18011, asv_16s_12827, asv_16s_18905, asv_16s_17203, asv_16s_17051, asv_16s_19000, asv_16s_18177, asv_16s_18376, asv_16s_17723, asv_16s_18085, asv_16s_18599, asv_16s_15543, asv_16s_17460, asv_16s_17616, asv_16s_17504, asv_16s_17556, asv_16s_17173, asv_16s_10117, asv_16s_9919, asv_16s_18761, asv_16s_18748, asv_16s_19025, asv_16s_11110, asv_16s_17449, asv_16s_18348, asv_16s_8949, asv_16s_14811, asv_16s_18497, asv_16s_18022, asv_16s_16414, asv_16s_18853, asv_16s_18477, asv_16s_13900, asv_16s_17785, asv_16s_17373, asv_16s_18998, asv_16s_9537, asv_16s_18766, asv_16s_18631, asv_16s_19026, asv_16s_18859, asv_16s_18732, asv_16s_10723, asv_16s_17791, asv_16s_17443, asv_16s_17410, asv_16s_18515, asv_16s_17983, asv_16s_19005, asv_16s_18336, asv_16s_16040, asv_16s_15735, asv_16s_18652

# step 2: hidden state prediction
hsp.py -i 16S -t placed_seqs.tre -o marker_nsti_predicted.tsv.gz -p 38 -n
hsp.py -i KO -t placed_seqs.tre -o KO_predicted.tsv.gz -p 38
hsp.py -i EC -t placed_seqs.tre -o EC_predicted.tsv.gz -p 38

# run prediction
metagenome_pipeline.py -i asv_for_picrust.txt -m marker_nsti_predicted.tsv.gz -f KO_predicted.tsv.gz --strat_out -o KO_metagenome_out 
# 514 of 18189 ASVs were above the max NSTI cut-off of 2.0 and were removed from the downstream analyses.

# also predict EC numbers for use with metacyc later
metagenome_pipeline.py -i asv_for_picrust.txt -m marker_nsti_predicted.tsv.gz -f EC_predicted.tsv.gz --strat_out -o EC_metagenome_out
# 514 of 18189 ASVs were above the max NSTI cut-off of 2.0 and were removed from the downstream analyses.

# summarize by metacyc pathway (both stratified and unstratified output
pathway_pipeline.py -i EC_metagenome_out/pred_metagenome_contrib.tsv.gz -o pathways_out_contrib --intermediate minpath_working -p 24
# /bio/Software/anaconda3/envs/picrust2-2.5.1/lib/python3.8/site-packages/picrust2/pathway_pipeline.py:642: FutureWarning: The operation <function sum at 0x7f82d44dd670> failed on a column. If any error is raised, this will raise an exception in a future version of pandas. Drop these columns to avoid this warning.
# contrib_path = pd.pivot_table(data=reaction_abun,

# add description
# that does not work for the stratified output
add_descriptions.py -i KO_metagenome_out/pred_metagenome_unstrat.tsv.gz -m KO -o KO_metagenome_out/pred_metagenome_unstrat_descrip.tsv.gz
add_descriptions.py -i EC_metagenome_out/pred_metagenome_unstrat.tsv.gz -m EC -o EC_metagenome_out/pred_metagenome_unstrat_descrip.tsv.gz
add_descriptions.py -i pathways_out_contrib/path_abun_unstrat.tsv.gz -m METACYC -o pathways_out_contrib/path_abun_unstrat_descrip.tsv.gz

# work with:
#   marker_nsti_predicted.tsv.gz: check which sequences were not used in the prediction (what is their proportion in the community)
#   pred_metagenome_unstrat_descrip.tsv.gz: available for KO and EC based functional prediction (there is also a description file associated with these table giving the names of the functions)
#   path_abun_unstrat.tsv.gz: pathway level metabolic predictions (+description file for pathways)

# see also: https://github.com/picrust/picrust2/wiki/Full-pipeline-script


################################################################
# copy number correction and metabolic prediction with paprica #
################################################################

cd /bio/Analysis_data/IOWseq000016_PaintSed1/Intermediate_results/metabolic_prediction/paprica_out

# step 1: prepare input files

# format mothur compatible count table
# this is an alternative to the scripts provided in the paprica workflow
paste <(cut -f1 ../picrust_out/asv_for_picrust.txt | sed 's/asv_id/Representative_Sequence/') <(cut -f2- ../picrust_out/asv_for_picrust.txt | sed '1d' | awk '{sum=0; for(i=1; i<=NF; i++) sum += $i; print sum}' | sed '1i total') | paste - <(cut -f2- ../picrust_out/asv_for_picrust.txt) > asv_for_paprica.count_table

# extract sequences
cut -f1 asv_for_paprica.count_table | sed '1d' | grep -A1 -w -F -f - ../../run1_16S/seqs_PaintSed1.fasta | sed '/--/d' > seqs_for_paprica.fasta

# generate a redundant sequence set
module load mothur/1.48.0
mothur "#deunique.seqs(fasta=seqs_for_paprica.fasta, count=asv_for_paprica.count_table)"
# split into individual fasta files per sample
mothur "#split.groups(fasta=seqs_for_paprica.redundant.fasta, group=asv_for_paprica.redundant.groups)"

# create sample list for paprica
mkdir paprica_seqs
mv seqs_for_paprica.redundant.*.fasta paprica_seqs
cd paprica_seqs
ls -1 seqs_for_paprica.redundant.*.fasta | sed 's/.fasta//' > sample_names_paprica.txt

# run paprica (use screen)
module load paprica/0.7.2
conda activate paprica-env
while read line
do
  time paprica-run.sh ${line} bacteria > ${line}.log 2>&1
done < sample_names_paprica.txt

# combine output tables
paprica-combine_results.py -domain bacteria -o paprica_PaintSed1

# work with:
#   paprica_PaintSed1.bacteria.path_tally.csv
#   paprica_PaintSed1.bacteria.ec_tally.csv

# see also: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8672035/